#NEXCRM


[Symfony Best Practices](https://symfony.com/doc/3.4/best_practices/index.html)

## Start

### Install

```bash
composer install
yarn install
php bin/console ckeditor:install
php bin/console assets:install
php bin/console nexcrm:base:generate-entities
php bin/console doctrine:database:create
php bin/console doc:schem:upd --force
php bin/console doctrine:fixtures:load
```

### Update

```bash
composer update
yarn install
php bin/console assets:install
php bin/console nexcrm:base:generate-entities
php bin/console doctrine:schema:drop --force
php bin/console doc:schem:upd --force
php bin/console doctrine:fixtures:load
```

```bash
- Installation request for nexcrm/base-bundle dev-master -> satisfiable
 by nexcrm/base-bundle[dev-master].
    - nexcrm/web-bundle dev-master requires nexcrm/base-bundle 0.3.* -> sat
isfiable by nexcrm/base-bundle[v0.3.x-dev].
```

`web-bundle dev-master` musí requierovat verze ostatních bundlů také
`dev-master`. Oprava: pushnout web-bundle dev-master s opraveným `composer.json`.

```bash
Fatal error: Uncaught Symfony\Component\Debug\Exception\ClassNotFoundExcept
ion: Attempted to load class "WebBundle" from namespace "NexCRM\WebBundle".
```

Spustí se symfony scripty před nainstalováním nových balíčků composerem. Oprava:
odstranit z `composer.json` celou sekci `scripts`, spustit znovu `composer update`.
Přidat zpět odstraněnou sekci `scripts` a opětovně spustit `composer update`.

### Build frontend

```bash
yarn run encore dev
yarn run encore dev --watch
```

```yarn run encore production```

### Test
```php bin/phpunit``` nebo **phpstorm**

### Run
```php bin/console server:run ```

## Dependency configs
- npm/yarn - [package.json](./package.json)
- composer - [composer.json](./composer.json)
- encore webpack - [webpack.config.js](./webpack.config.js)
- postcss: autoprefixing - [postcss.config.js](./postcss.config.js)
- ReactBundle - [config.yml](./app/config/config.yml)

## Symfony bundles

###[Symfony Encore](https://symfony.com/doc/master/frontend.html)
Usnadnuje konfiguraci a praci s webpackem.

###[ReactBundle](https://github.com/Limenius/ReactBundle)
Umoznuje registrovat react componenty a vkladat pomoci twigu `{{ react_component('Component', {'trace': true}) }}` 
(trace zapina vypis console.logu do console pri serverside renderovani).

### Buddy
Používá jiný composer pro instalaci: composer-buddy.json.
