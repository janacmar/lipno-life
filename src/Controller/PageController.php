<?php

namespace App\Controller;

use App\Service\MetaTagsService;
use NexCRM\WebBundle\Entity\Page;
use NexCRM\WebBundle\Entity\SEOTranslation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends BaseController
{
    /**
     * @Route("/{_locale}", name="front_homepage", defaults={"_locale": "cs"}, requirements={
     *     "_locale": "([a-z]){2}"
     * })
     */
    public function indexAction(Request $request)
    {
        $message = $request->query->get("message");
        $this->initialize($request);
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(["homepage" => 1]);

        return $this->render('front/index.html.twig', [
            "meta" => $this->metaTagsService->getMetaTags($page),
            "page" => $page,
            "response" => $message,
            "langItem" => $page
        ]);
    }

    public function pageAction(Request $request)
    {
        $message = $request->query->get("message");
        $routeId = explode('_', $request->get('_route'));
        $this->initialize($request);

        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['seo' => $routeId[1]]);
        if (!$page->getId()) {
            return new Response('', 404, array('Content-Type' => 'text/html'));
        }

        $template_uri = '';
        if (!empty($page->getTemplate())) {
            $template_uri = 'front/' . $page->getTemplate()->getPath() . '.html.twig';
        }
        if (!$this->get('twig')->getLoader()->exists($template_uri)) {
            $template_uri = 'front/page_default.html.twig';
        }

        return $this->render($template_uri, [
            "meta" => $this->metaTagsService->getMetaTags($page),
            "page" => $page,
            "response" => $message,
            "langItem" => $page
        ]);
    }

}
