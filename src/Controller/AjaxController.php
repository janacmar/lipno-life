<?php

namespace App\Controller;

use App\Entity\Parcel;
use NexCRM\BaseBundle\Service\Translator;
use NexCRM\WebBundle\Entity\News;
use NexCRM\WebBundle\Entity\Page;
use NexCRM\WebBundle\Entity\SEOTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NexCRM\WebBundle\Entity\Contact;
use NexCRM\WebBundle\Form\ContactType;


class AjaxController extends AbstractController
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/{_locale}/ajax/contact/{id}/{redirect}", name="ajax_contact", defaults={"_locale" = "cs"})
     */
    public function ajaxContactAction(Request $request, $_locale, $id, $redirect)
    {
        $this->translator->setLocale($_locale);
        $parcel = $this->getDoctrine()->getRepository(Parcel::class)->find($id);
        $contact_form_e = new Contact();
        $contact_form = $this->createForm(ContactType::class, $contact_form_e, [
            'action' => $this->generateUrl('contact_form', ["redirect" => $redirect ]),
        ]);
        return new Response($this->renderView('front/component/popup-form.html.twig', [
                    "parcel" => $parcel,
                    "contactForm" => $contact_form->createView(),
                    "lang" => $request->getLocale(),
                    "isAjax" => true
                ]
        ));
    }
}
