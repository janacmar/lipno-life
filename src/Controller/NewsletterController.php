<?php

namespace App\Controller;

use App\Entity\Newsletter;
use Doctrine\ORM\EntityManager;
use NexCRM\BaseBundle\Service\Translator;
use NexCRM\WebBundle\Entity\Setting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class NewsletterController extends AbstractController
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private $secret = "sekretGDPR";

    public function __construct(
        Translator $translator,
        EntityManager $entityManager,
        Swift_Mailer $mailer
    )
    {
        $this->translator = $translator;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{_locale}/newsletter/{redirect}", name="newsletter", defaults={"_locale" = "cs"})
     */
    public function indexAction(Request $request, $redirect, $_locale)
    {
        $this->translator->setLocale($_locale);

        $email = $request->query->get('email');
        $antispam = $request->query->get('antispam');

        if (!empty($email) && $antispam == 'antispam') {
            $hash = md5($email . $this->secret);
            $optInUrl = $this->generateUrl('newsletter_opt_in', [
                'email' => $email,
                'lang' => $_locale,
                'hash' => $hash,
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            $setting_email = $this->getDoctrine()->getRepository(\NexCRM\BaseBundle\Entity\Setting::class)->findOneBy(['alias' => 'email']);
            $message = $this->render('email/newsletter.html.twig', ['optInUrl' => $optInUrl])->getContent();

            $email_send_from = 'info@lipno-real.com';
            if ($setting_email->translate($_locale)->getTransValue()) {
                $email_send_from = $setting_email->translate($_locale)->getTransValue();
            }

            $message_mail = (new Swift_Message())
                ->setSubject($this->translator->trans("Odběr novinek lipno-life.com"))
                ->setFrom($email_send_from)
                ->setTo($email)
                ->setBody($message, 'text/html');
            $this->mailer->send($message_mail);

            $this->addFlash(
                'success',
                $this->translator->trans("Na zadanou e-mailovou adresu byl odeslán odkaz pro přihlášení odběru novinek.")
            );
        } else {
            $this->addFlash(
                'error',
                $this->translator->trans("Chyba během odesílání. Zkuste to prosím později.")
            );
        }

        return $this->redirectToRoute($redirect);
    }

    /**
     * @Route("/{_locale}/newsletter-opt-in", name="newsletter_opt_in", defaults={"_locale" = "cs"})
     */
    public function newsletterOptInAction(Request $request, $_locale)
    {
        $this->translator->setLocale($_locale);

        $email = $request->query->get('email');
        $hash = $request->query->get('hash');
        $lang = $request->query->get('lang');

        if (!empty($hash) && !empty($email) && $hash == md5($email . $this->secret)) {

            $emailInDb = $this->entityManager->getRepository(Newsletter::class)->findOneBy(['email' => $email]);
            if ($emailInDb) {
                $this->addFlash(
                    'success',
                    $this->translator->trans("E-mail již v naší databázi existuje.")
                );
            } else {
                $newsletter = new Newsletter();
                $newsletter->setEmail($email);
                $newsletter->setCreated(new \DateTime());
                $newsletter->setLang($lang);
                $this->entityManager->persist($newsletter);
                $this->entityManager->flush();
                $this->addFlash(
                    'success',
                    $this->translator->trans("Děkujeme. Přihlášení k odběru novinek bylo úspěšné.")
                );
            }

        } else {
            $this->addFlash(
                'error',
                $this->translator->trans("Chyba během ukládání. Zkuste to prosím později.")
            );
        }

        return $this->redirectToRoute('front_homepage');
    }
}
