<?php

namespace App\Controller;

use App\Entity\Parcel;
use App\Entity\SummerActivity;
use App\Entity\WinterActivity;
use App\Entity\Slider;
use App\Service\MetaTagsService;
use App\Service\PageTemplateService;
use Doctrine\ORM\EntityManager;
use NexCRM\WebBundle\Entity\Contact;
use NexCRM\WebBundle\Entity\News;
use NexCRM\WebBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NexCRM\BaseBundle\Entity\Setting;
use NexCRM\WebBundle\Entity\Page;
use NexCRM\WebBundle\Entity\Block;
use NexCRM\WebBundle\Entity\Menu;
use NexCRM\BaseBundle\Entity\Lang;
use NexCRM\WebBundle\Entity\SEOTranslation;
use NexCRM\BaseBundle\Service\Translator;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class BaseController extends AbstractController
{

    protected $data;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var PageTemplateService
     */
    private $pageTemplateService;
    /**
     * @var MetaTagsService
     */
    public $metaTagsService;

    public \Swift_Mailer $mailer;

    /**
     * BaseController constructor.
     *
     * @param Translator $translator
     * @param EntityManager $entityManager
     * @param PageTemplateService $pageTemplateService
     * @param MetaTagsService $metaTagsService
     * @param \Swift_Mailer $mailer
     */
    public function __construct(
        Translator $translator,
        EntityManager $entityManager,
        PageTemplateService $pageTemplateService,
        MetaTagsService $metaTagsService,
        \Swift_Mailer $mailer
    )
    {
        $this->data = [];
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->pageTemplateService = $pageTemplateService;
        $this->metaTagsService = $metaTagsService;
        $this->mailer = $mailer;
    }

    public function render(string $view, array $parameters = [], Response $response = NULL): Response
    {
        return parent::render($view, array_merge((array)$this->data, $parameters, [
            'base_dir' => realpath($this->getParameter('kernel.project_dir') . '/'),
        ]), $response);
    }

    public function initialize(Request $request)
    {
        $this->translator->setLocale($request->getLocale());

        $limit = [
            "news" => 6,
            "references" => 16
        ];

        $paginatorPage = $request->get("page", 1);

        $settings = $this->getDoctrine()->getRepository(Setting::class)->findByToArray();
        $AppType = $this->getDoctrine()->getRepository(\NexCRM\BaseBundle\Entity\Setting::class)->findOneBy(['alias' => 'App_type']);
        $pages = $this->getDoctrine()->getRepository(Page::class)->findBy([]);
        $menus = $this->getDoctrine()->getRepository(Menu::class)->findByGroupOrdered();
        $blocks = $this->getDoctrine()->getRepository(Block::class)->findByToArray();
        $page_templates = $this->pageTemplateService->getPagetemplate($pages);
        $news = $this->getDoctrine()->getRepository(News::class)->findVisible($limit['news'], ($paginatorPage - 1) * $limit['news']);
        $parcels = $this->getDoctrine()->getRepository(Parcel::class)->findBy([], ['sort' => 'ASC']);
        $summer_activities = $this->getDoctrine()->getRepository(SummerActivity::class)->findBy([], ['sort' => 'ASC']);
        $winter_activities = $this->getDoctrine()->getRepository(WinterActivity::class)->findBy([], ['sort' => 'ASC']);
        $sliders = $this->getDoctrine()->getRepository(Slider::class)->findBy([], ['sort' => 'ASC']);

        $contact_form_e = new Contact();
        $contact_form = $this->createForm(ContactType::class, $contact_form_e, [
            'action' => $this->generateUrl('contact_form', ["redirect" => $request->get('_route')]),
        ]);

        $this->data["contactForm"] = $contact_form->createView();
        $this->data["settings"] = $settings;
        $this->data["pages"] = $pages;
        $this->data["menus"] = $menus;
        $this->data["blocks"] = $blocks;
        $this->data["sliders"] = $sliders;
        $this->data["paginatorPage"] = $paginatorPage;
        $this->data["limit"] = $limit;
        $this->data["pt"] = $page_templates;
        $this->data["news"] = $news;
        $this->data["lang"] = $request->getLocale();
        $this->data["AppType"] = $AppType;
        $this->data["parcels"] = $parcels;
        $this->data["summer_activities"] = $summer_activities;
        $this->data["winter_activities"] = $winter_activities;
        $this->data["sliders"] = $sliders;
    }

    /**
     * @Route("/{_locale}", name="front_homepage", defaults={"_locale": "cs"}, requirements={
     *     "_locale": "([a-z]){2}"
     * })
     */
    public function indexAction(Request $request)
    {

    }

    /**
     * @Route("/{_locale}/contact-form/{redirect}", name="contact_form", defaults={"_locale" = "cs"})
     */
    public function contactFormAction(Request $request, $redirect, $_locale)
    {
        $this->translator->setLocale($_locale);

        $contactForm = new Contact();
        $form = $this->createForm(ContactType::class, $contactForm);
        $form->handleRequest($request);
        $this->translator->setLocale($request->getLocale());


        if ($form->isSubmitted() && $form->isValid() && $contactForm->getAntispam() == "antispam") {
            $em = $this->getDoctrine()->getManager();
            $contactForm->setSended(new \DateTime());
            $contactForm->setIp($request->getClientIp());
            $em->persist($contactForm);
            $em->flush();

            $setting_email = $this->getDoctrine()->getRepository(\NexCRM\BaseBundle\Entity\Setting::class)->findOneBy(['alias' => 'email']);
            $message = $this->render('email/contact-form.html.twig', ['form' => $form])->getContent();

            $email_send_from = 'info@lipno-real.com';
            if ($setting_email->translate($_locale)->getTransValue()) {
                $email_send_from = $setting_email->translate($_locale)->getTransValue();
            }
            $email_send_to = $email_send_from;

            $message_mail = new \Swift_Message();
            $message_mail
                ->setSubject($contactForm->getSubject())
                ->setFrom($email_send_from)
                ->setTo($email_send_to)
                ->setBody($message, 'text/html');
            $this->mailer->send($message_mail);
            $this->addFlash(
                'success',
                $this->translator->trans("E-mail byl úspěšně odeslán.")
            );

        } else {
            $this->addFlash(
                'error',
                $this->translator->trans('Chyba během odesílání. Zkuste to prosím později.')
            );
        }

        return $this->redirectToRoute($redirect);
    }
}
