<?php

namespace App\Controller\Admin;

use NexCRM\BaseBundle\CRUD\AbstractController;
use NexCRM\BaseBundle\CRUD\EntityConfiguration;
use Doctrine\ORM\EntityManager;
use App\Entity\WinterActivity;
use App\Form\WinterActivityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Breadcrumb({
 *   { "label" = "$entityConfiguration.label[index]", "route" = "nexcrm_module_winter_activity_index" }
 * })
 */
class WinterActivityController extends AbstractController
{

    public function __construct(EntityManager $entityManager)
    {
        $entityConfiguration = (new EntityConfiguration())
            ->setName(WinterActivity::class)
            ->setKey("winter_activity")
            ->setFormType(WinterActivityType::class)
            ->setProperties([
                "translate.name" => "Název"
            ])

            ->setSearch([
                "translations.name"
            ])
            ->setFilters([

            ])

            ->setLabel([
                "index" => "Zimní aktivity",
                "new" => "Nová zimní aktivita",
                "show" => "Zobrazení zimní aktivity",
                "edit" => "Úprava zimní aktivity",
                "notFound" => "Žádná zimní aktivita nenalezena.",
                "delete" => "Opravdu chcete zimní aktivitu smazat?",
                "module" => "winter_activity"
            ]);

        parent::__construct($entityManager, $entityConfiguration);
    }

    /**
     * @Security("is_granted('ROLE_WINTER_ACTIVITY_TEMPLATE_LIST')")
     *
     * @Route("/winter_activity", name="nexcrm_module_winter_activity_index")
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return $this->list($request, 'activity/index.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[new]" }
     * })
     *
     * @Security("is_granted('ROLE_WINTER_ACTIVITY_TEMPLATE_NEW')")
     *
     * @Route("/winter_activity/new", name="nexcrm_module_winter_activity_new")
     */
    public function newAction(Request $request)
    {
        return $this->new($request, 'activity/new.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[show]" }
     * })
     *
     * @Security("is_granted('ROLE_WINTER_ACTIVITY_TEMPLATE_SHOW')")
     *
     * @Route("/winter_activity/show/{entity_id}", name="nexcrm_module_winter_activity_show")
     */
    public function showAction(Request $request, $entity_id)
    {
        return $this->show($request, 'activity/show.html.twig', $entity_id);
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[edit]" }
     * })
     *
     * @Security("is_granted('ROLE_WINTER_ACTIVITY_TEMPLATE_EDIT')")
     *
     * @Route("/winter_activity/edit/{entity_id}", name="nexcrm_module_winter_activity_edit")
     */
    public function editAction(Request $request, $entity_id)
    {
        return $this->edit($request, 'activity/edit.html.twig', $entity_id);
    }

    /**
     * @Security("is_granted('ROLE_WINTER_ACTIVITY_TEMPLATE_REMOVE')")
     *
     * @Route("/winter_activity/remove/{entity_id}", name="nexcrm_module_winter_activity_remove")
     */
    public function removeAction(Request $request, $entity_id)
    {
        return $this->remove($request, $entity_id);
    }
}
