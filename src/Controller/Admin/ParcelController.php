<?php

namespace App\Controller\Admin;

use NexCRM\BaseBundle\CRUD\AbstractController;
use NexCRM\BaseBundle\CRUD\EntityConfiguration;
use Doctrine\ORM\EntityManager;
use App\Entity\Parcel;
use App\Form\ParcelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Breadcrumb({
 *   { "label" = "$entityConfiguration.label[index]", "route" = "nexcrm_module_parcel_index" }
 * })
 */
class  ParcelController extends AbstractController
{

    public function __construct(EntityManager $entityManager)
    {
        $entityConfiguration = (new EntityConfiguration())
            ->setName( Parcel::class)
            ->setKey("parcel")
            ->setFormType( ParcelType::class)
            ->setProperties([
                "name" => "Název",
                'totalArea' => 'Velikost pozemku',
                'usableArea' => 'Zastavitelná plocha',
                'state' => 'Stav'
            ])

            ->setSearch([
                "name"
            ])

            ->setLabel([
                "index" => "Parcely",
                "new" => "Nová parcela",
                "show" => "Zobrazení parcely",
                "edit" => "Úprava parcely",
                "notFound" => "Žádná parcela nenalezena.",
                "delete" => "Opravdu chcete parcelu smazat?",
                "module" => "parcel"
            ]);

        parent::__construct($entityManager, $entityConfiguration);
    }

    /**
     * @Security("is_granted('ROLE_PARCEL_TEMPLATE_LIST')")
     *
     * @Route("/parcel", name="nexcrm_module_parcel_index")
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return $this->list($request, 'parcel/index.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[new]" }
     * })
     *
     * @Security("is_granted('ROLE_PARCEL_TEMPLATE_NEW')")
     *
     * @Route("/parcel/new", name="nexcrm_module_parcel_new")
     */
    public function newAction(Request $request)
    {
        return $this->new($request, 'parcel/new.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[show]" }
     * })
     *
     * @Security("is_granted('ROLE_PARCEL_TEMPLATE_SHOW')")
     *
     * @Route("/parcel/show/{entity_id}", name="nexcrm_module_parcel_show")
     */
    public function showAction(Request $request, $entity_id)
    {
        return $this->show($request, 'parcel/show.html.twig', $entity_id);
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[edit]" }
     * })
     *
     * @Security("is_granted('ROLE_PARCEL_TEMPLATE_EDIT')")
     *
     * @Route("/parcel/edit/{entity_id}", name="nexcrm_module_parcel_edit")
     */
    public function editAction(Request $request, $entity_id)
    {
        return $this->edit($request, 'parcel/edit.html.twig', $entity_id);
    }

    /**
     * @Security("is_granted('ROLE_PARCEL_TEMPLATE_REMOVE')")
     *
     * @Route("/parcel/remove/{entity_id}", name="nexcrm_module_parcel_remove")
     */
    public function removeAction(Request $request, $entity_id)
    {
        return $this->remove($request, $entity_id);
    }
}
