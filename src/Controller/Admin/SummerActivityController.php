<?php

namespace App\Controller\Admin;

use NexCRM\BaseBundle\CRUD\AbstractController;
use NexCRM\BaseBundle\CRUD\EntityConfiguration;
use Doctrine\ORM\EntityManager;
use App\Entity\SummerActivity;
use App\Form\SummerActivityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Breadcrumb({
 *   { "label" = "$entityConfiguration.label[index]", "route" = "nexcrm_module_summer_activity_index" }
 * })
 */
class SummerActivityController extends AbstractController
{

    public function __construct(EntityManager $entityManager)
    {
        $entityConfiguration = (new EntityConfiguration())
            ->setName(SummerActivity::class)
            ->setKey("summer_activity")
            ->setFormType(SummerActivityType::class)
            ->setProperties([
                "translate.name" => "Název"
            ])

            ->setSearch([
                "translations.name"
            ])
            ->setFilters([

            ])

            ->setLabel([
                "index" => "Letní aktivity",
                "new" => "Nová letní aktivita",
                "show" => "Zobrazení letní aktivity",
                "edit" => "Úprava letní aktivity",
                "notFound" => "Žádná letní aktivita nenalezena.",
                "delete" => "Opravdu chcete letní aktivitu smazat?",
                "module" => "summer_activity"
            ]);

        parent::__construct($entityManager, $entityConfiguration);
    }

    /**
     * @Security("is_granted('ROLE_SUMMER_ACTIVITY_TEMPLATE_LIST')")
     *
     * @Route("/summer_activity", name="nexcrm_module_summer_activity_index")
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return $this->list($request, 'activity/index.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[new]" }
     * })
     *
     * @Security("is_granted('ROLE_SUMMER_ACTIVITY_TEMPLATE_NEW')")
     *
     * @Route("/summer_activity/new", name="nexcrm_module_summer_activity_new")
     */
    public function newAction(Request $request)
    {
        return $this->new($request, 'activity/new.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[show]" }
     * })
     *
     * @Security("is_granted('ROLE_SUMMER_ACTIVITY_TEMPLATE_SHOW')")
     *
     * @Route("/summer_activity/show/{entity_id}", name="nexcrm_module_summer_activity_show")
     */
    public function showAction(Request $request, $entity_id)
    {
        return $this->show($request, 'activity/show.html.twig', $entity_id);
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[edit]" }
     * })
     *
     * @Security("is_granted('ROLE_SUMMER_ACTIVITY_TEMPLATE_EDIT')")
     *
     * @Route("/summer_activity/edit/{entity_id}", name="nexcrm_module_summer_activity_edit")
     */
    public function editAction(Request $request, $entity_id)
    {
        return $this->edit($request, 'activity/edit.html.twig', $entity_id);
    }

    /**
     * @Security("is_granted('ROLE_SUMMER_ACTIVITY_TEMPLATE_REMOVE')")
     *
     * @Route("/summer_activity/remove/{entity_id}", name="nexcrm_module_summer_activity_remove")
     */
    public function removeAction(Request $request, $entity_id)
    {
        return $this->remove($request, $entity_id);
    }
}
