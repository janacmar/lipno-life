<?php

namespace App\Controller\Admin;

use App\Entity\Newsletter;
use Doctrine\ORM\EntityManager;
use NexCRM\BaseBundle\Controller\AbstractBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractBaseController
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/download-newsletter", name="nexcrm_admin_download_newsletter")
     */
    public function downloadNewsletterAction(Request $request): Response
    {
        $items = $this->entityManager->getRepository(Newsletter::class)->findAll();

        $emails = [];
        foreach ($items as $list) {
            $emails[][] = $list->getEmail();
        }

        return $this->responseCsv('newsletter.csv', $emails);
    }

    private function responseCsv($filename, $data)
    {
        $fp = fopen('php://output', 'w');

        foreach ($data as $item) {
            fputcsv($fp, $item);
        }

        fclose($fp);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="lipno_life_' . $filename . '"');
        return $response;
    }
}
