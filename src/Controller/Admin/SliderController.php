<?php

namespace App\Controller\Admin;

use NexCRM\BaseBundle\CRUD\AbstractController;
use NexCRM\BaseBundle\CRUD\EntityConfiguration;
use Doctrine\ORM\EntityManager;
use App\Entity\Slider;
use App\Form\SliderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Breadcrumb({
 *   { "label" = "$entityConfiguration.label[index]", "route" = "nexcrm_module_slider_index" }
 * })
 */
class SliderController extends AbstractController
{

    public function __construct(EntityManager $entityManager)
    {
        $entityConfiguration = (new EntityConfiguration())
            ->setName(Slider::class)
            ->setKey("slider")
            ->setFormType(SliderType::class)
            ->setProperties([
                "name" => "Název"
            ])

            ->setSearch([
                "translations.title"
            ])

            ->setLabel([
                "index" => "Slidery",
                "new" => "Nový slider",
                "show" => "Zobrazení slideru",
                "edit" => "Úprava slideru",
                "notFound" => "Žádný slider nenalezen.",
                "delete" => "Opravdu chcete slider smazat?",
                "module" => "slider"
            ]);

        parent::__construct($entityManager, $entityConfiguration);
    }

    /**
     * @Security("is_granted('ROLE_SLIDER_TEMPLATE_LIST')")
     *
     * @Route("/slider", name="nexcrm_module_slider_index")
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return $this->list($request, 'slider/index.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[new]" }
     * })
     *
     * @Security("is_granted('ROLE_SLIDER_TEMPLATE_NEW')")
     *
     * @Route("/slider/new", name="nexcrm_module_slider_new")
     */
    public function newAction(Request $request)
    {
        return $this->new($request, 'slider/new.html.twig');
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[show]" }
     * })
     *
     * @Security("is_granted('ROLE_SLIDER_TEMPLATE_SHOW')")
     *
     * @Route("/slider/show/{entity_id}", name="nexcrm_module_slider_show")
     */
    public function showAction(Request $request, $entity_id)
    {
        return $this->show($request, 'slider/show.html.twig', $entity_id);
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[edit]" }
     * })
     *
     * @Security("is_granted('ROLE_SLIDER_TEMPLATE_EDIT')")
     *
     * @Route("/slider/edit/{entity_id}", name="nexcrm_module_slider_edit")
     */
    public function editAction(Request $request, $entity_id)
    {
        return $this->edit($request, 'slider/edit.html.twig', $entity_id);
    }

    /**
     * @Security("is_granted('ROLE_SLIDER_TEMPLATE_REMOVE')")
     *
     * @Route("/slider/remove/{entity_id}", name="nexcrm_module_slider_remove")
     */
    public function removeAction(Request $request, $entity_id)
    {
        return $this->remove($request, $entity_id);
    }
}
