<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813070642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, winter TINYINT(1) NOT NULL, INDEX IDX_AC74095A93CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_BAE72F632C2AC5D3 (translatable_id), UNIQUE INDEX activity_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A93CB796C FOREIGN KEY (file_id) REFERENCES media_file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE activity_translation ADD CONSTRAINT FK_BAE72F632C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity_translation DROP FOREIGN KEY FK_BAE72F632C2AC5D3');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE activity_translation');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }
}
