<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813225900 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710074C29F1B2');
        $this->addSql('DROP INDEX IDX_CFC710074C29F1B2 ON slider');
        $this->addSql('ALTER TABLE slider CHANGE doc_file_id page_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC7100769AB92F9 FOREIGN KEY (page_file_id) REFERENCES media_file (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFC7100769AB92F9 ON slider (page_file_id)');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC7100769AB92F9');
        $this->addSql('DROP INDEX IDX_CFC7100769AB92F9 ON slider');
        $this->addSql('ALTER TABLE slider CHANGE page_file_id doc_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710074C29F1B2 FOREIGN KEY (doc_file_id) REFERENCES dms_file (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFC710074C29F1B2 ON slider (doc_file_id)');
    }
}
