<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813220250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slider (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, doc_file_id INT DEFAULT NULL, INDEX IDX_CFC7100793CB796C (file_id), INDEX IDX_CFC710074C29F1B2 (doc_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slider_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, subtitle VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_CDA703942C2AC5D3 (translatable_id), UNIQUE INDEX slider_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC7100793CB796C FOREIGN KEY (file_id) REFERENCES media_file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710074C29F1B2 FOREIGN KEY (doc_file_id) REFERENCES dms_file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE slider_translation ADD CONSTRAINT FK_CDA703942C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES slider (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider_translation DROP FOREIGN KEY FK_CDA703942C2AC5D3');
        $this->addSql('DROP TABLE slider');
        $this->addSql('DROP TABLE slider_translation');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }
}
