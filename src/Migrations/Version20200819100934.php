<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200819100934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity_translation DROP FOREIGN KEY FK_BAE72F632C2AC5D3');
        $this->addSql('CREATE TABLE summer_activity (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, winter TINYINT(1) NOT NULL, sort INT NOT NULL, INDEX IDX_E69E8E9793CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE summer_activity_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_414AC2A02C2AC5D3 (translatable_id), UNIQUE INDEX summer_activity_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE winter_activity (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, winter TINYINT(1) NOT NULL, sort INT NOT NULL, INDEX IDX_CDEFD1693CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE winter_activity_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_EC4AD9082C2AC5D3 (translatable_id), UNIQUE INDEX winter_activity_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE summer_activity ADD CONSTRAINT FK_E69E8E9793CB796C FOREIGN KEY (file_id) REFERENCES media_file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE summer_activity_translation ADD CONSTRAINT FK_414AC2A02C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES summer_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE winter_activity ADD CONSTRAINT FK_CDEFD1693CB796C FOREIGN KEY (file_id) REFERENCES media_file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE winter_activity_translation ADD CONSTRAINT FK_EC4AD9082C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES winter_activity (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE activity_translation');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE summer_activity_translation DROP FOREIGN KEY FK_414AC2A02C2AC5D3');
        $this->addSql('ALTER TABLE winter_activity_translation DROP FOREIGN KEY FK_EC4AD9082C2AC5D3');
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, winter TINYINT(1) NOT NULL, sort INT NOT NULL, season LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT \'(DC2Type:array)\', INDEX IDX_AC74095A93CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE activity_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, link VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, locale VARCHAR(5) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, UNIQUE INDEX activity_translation_unique_translation (translatable_id, locale), INDEX IDX_BAE72F632C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A93CB796C FOREIGN KEY (file_id) REFERENCES media_file (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE activity_translation ADD CONSTRAINT FK_BAE72F632C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES activity (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE summer_activity');
        $this->addSql('DROP TABLE summer_activity_translation');
        $this->addSql('DROP TABLE winter_activity');
        $this->addSql('DROP TABLE winter_activity_translation');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }
}
