<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200814004529 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity ADD sort INT NOT NULL');
        $this->addSql('ALTER TABLE parcel ADD sort INT NOT NULL');
        $this->addSql('ALTER TABLE slider ADD sort INT NOT NULL');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity DROP sort');
        $this->addSql('DROP INDEX folder_unique ON folder');
        $this->addSql('CREATE UNIQUE INDEX folder_unique ON folder (name, root, lvl, lft, rgt)');
        $this->addSql('ALTER TABLE parcel DROP sort');
        $this->addSql('ALTER TABLE slider DROP sort');
    }
}
