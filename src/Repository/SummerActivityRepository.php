<?php

namespace App\Repository;

use App\Entity\SummerActivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use NexCRM\BaseBundle\Repository\BaseRepository;


/**
 * @method SummerActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method SummerActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method SummerActivity[]    findAll()
 * @method SummerActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SummerActivityRepository extends BaseRepository
{
    // /**
    //  * @return Activity[] Returns an array of Activity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Activity
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
