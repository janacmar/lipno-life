<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;
use NexCRM\WebBundle\Entity\Setting;

class MetaTagsService
{
    /**
     * @var EntityManager
     */
    private $entityManager;


    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getMetaTags($entity = null)
    {
        $settings = $this->entityManager->getRepository(Setting::class)->findByToArray();

        $meta = [
            'title' => isset($settings['title']) ? $settings['title'] : '',
            'description' => isset($settings['description']) ? $settings['description'] : '',
            'keywords' => isset($settings['keywords']) ? $settings['keywords'] : ''
        ];

        if (isset($entity)) {
            $m = $entity->getSeo()->translate();

            $suffix = ' | ' . $meta['title'];

            if (!empty($m->getTitle())) {
                $meta['title'] = $m->getTitle() . $suffix;
            } else {
                $meta['title'] = $entity->translate()->getName() . $suffix;
            }

            if (!empty($m->getDescription())) {
                $meta['description'] = $m->getDescription();
            }

            if (!empty($m->getKeywords())) {
                $meta['keywords'] = $m->getKeywords();
            }
        }

        return $meta;
    }
}
