<?php

namespace App\Service;


class PageTemplateService
{
    public function getPagetemplate(array $pages)
    {
        $page_templates = [];
        foreach ($pages as $page) {
            if ($page->getTemplate()) {
                $page_templates[$page->getTemplate()->getPath()] = $page;
            }
        }

        return $page_templates;
    }

}
