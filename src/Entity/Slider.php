<?php

namespace App\Entity;

use App\Repository\SliderRepository;
use Doctrine\ORM\Mapping as ORM;
use NexCRM\BaseBundle\Entity\EntityTrait\IdentifiableTrait;
use NexCRM\BaseBundle\Entity\EntityTrait\MediaSingleTrait;
use NexCRM\BaseBundle\Entity\BaseEntity;
use NexCRM\BaseBundle\Entity\MediaFile;
use NexCRM\WebBundle\Entity\Page;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use NexCRM\DMSBundle\Entity\EntityTrait\DocFileSingleTrait;
use NexCRM\BaseBundle\Entity\EntityTrait\Sortable;


/**
 * @ORM\Entity(repositoryClass=SliderRepository::class)
 */
class Slider extends BaseEntity implements TranslatableInterface
{
    use IdentifiableTrait;
    use ORMBehaviors\Translatable\TranslatableTrait;
    use MediaSingleTrait;
    use Sortable;

    /**
     * @ORM\ManyToOne(targetEntity="NexCRM\BaseBundle\Entity\MediaFile", cascade={"persist"})
     * @ORM\JoinColumn(name="page_file_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $pageFile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /** @return MediaFile */
    public function getPageFile(): ?MediaFile
    {
        return $this->pageFile;
    }

    /**
     * @param MediaFile $pageFile
     *
     * @return self
     */
    public function setPageFile(?MediaFile $pageFile): self
    {
        $this->pageFile = $pageFile;

        return $this;
    }
}