<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NexCRM\BaseBundle\Entity\EntityTrait\IdentifiableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * WinterActivity
 *
 * @ORM\Table(name="winter_activity_translation")
 * @ORM\Entity(repositoryClass="App\Repository\WinterActivityRepository")
 */
class WinterActivityTranslation implements TranslationInterface
{
    use IdentifiableTrait;
    use ORMBehaviors\Translatable\TranslationTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message = "app.validation.notblank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "app.validation.notblank")
     */
    private $link;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     */
    public function setLink(?string $link): void
    {
        $this->link = $link;
    }
}
