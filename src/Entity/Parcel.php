<?php

namespace App\Entity;

use App\Repository\ParcelRepository;
use Doctrine\ORM\Mapping as ORM;
use NexCRM\BaseBundle\Entity\EntityTrait\IdentifiableTrait;
use NexCRM\BaseBundle\Entity\BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;
use NexCRM\BaseBundle\Entity\EntityTrait\Sortable;


/**
 * @ORM\Entity(repositoryClass=ParcelRepository::class)
 */
class Parcel extends BaseEntity
{
    use IdentifiableTrait;
    use Sortable;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "app.validation.notblank")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $totalArea;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usableArea;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    public function __toString()
    {
        return $this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTotalArea(): ?string
    {
        return $this->totalArea;
    }

    public function setTotalArea(?string $totalArea): self
    {
        $this->totalArea = $totalArea;

        return $this;
    }

    public function getUsableArea(): ?string
    {
        return $this->usableArea;
    }

    public function setUsableArea(?string $usableArea): self
    {
        $this->usableArea = $usableArea;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
}
