<?php

namespace App\Entity;

use App\Repository\SummerActivityRepository;
use Doctrine\ORM\Mapping as ORM;
use NexCRM\BaseBundle\Entity\EntityTrait\IdentifiableTrait;
use NexCRM\BaseBundle\Entity\EntityTrait\MediaSingleTrait;
use NexCRM\BaseBundle\Entity\BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use NexCRM\BaseBundle\Entity\EntityTrait\Sortable;


/**
 * @ORM\Entity(repositoryClass=SummerActivityRepository::class)
 */
class SummerActivity extends BaseEntity implements TranslatableInterface
{
    use IdentifiableTrait;
    use MediaSingleTrait;
    use ORMBehaviors\Translatable\TranslatableTrait;
    use Sortable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $winter;

    public function __toString()
    {
        return (string)$this->findTranslationByLocale($this->getCurrentLocale());
    }

    public function getWinter(): ?bool
    {
        return $this->winter;
    }

    public function setWinter(bool $winter): self
    {
        $this->winter = $winter;

        return $this;
    }
}
