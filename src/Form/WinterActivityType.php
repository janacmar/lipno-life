<?php

namespace App\Form;

use App\Entity\WinterActivity;
use Doctrine\ORM\EntityManagerInterface;
use NexCRM\BaseBundle\Form\MediaFileInterface;
use NexCRM\BaseBundle\Form\MediaFileMultipleType;
use NexCRM\BaseBundle\Form\MediaFileType;
use NexCRM\BaseBundle\Form\TabType;
use NexCRM\WebBundle\Form\SEOType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use NexCRM\BaseBundle\Service\Translator;
use Symfony\Component\HttpFoundation\RequestStack;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;

class WinterActivityType extends AbstractType
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var Translator
     */
    private $translator;


    public function __construct(
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        Translator $translator
    )
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $activity = $builder->create('tab_page', TabType::class, array(
            'label' => 'Obsah',
            'icon' => 'file',
            'inherit_data' => true,
            'tab_active' => true,
            'data_class' => WinterActivity::class
        ));

        $activity
            ->add('translations', TranslationsType::class, [
                'fields' => [
                    'name' => [
                        'label' => "Název",
                        'label_attr' => ['class' => 'required'],
                        'attr' => [
                            'class' => 'js-form-name-gen',
                            'required' => true,
                        ],
                    ],
                    'link' => [
                        'label' => "Odkaz",
                        'label_attr' => ['class' => 'required'],
                        'attr' => [
                            'required' => true,
                        ],
                    ],
                ],
                'label' => false,
            ])
            ->add('winter', HiddenType::class, [
                'empty_data' => true
            ]);

        $media = $builder->create('tab_media', TabType::class, array(
            'label' => 'Média',
            'icon' => 'image',
            'inherit_data' => true,
            'tab_active' => true,
        ));

        $media
            ->add('file', MediaFileType::class, array(
                "label" => "Obrázek",
                'required' => false,
                "allowed_filetypes" => MediaFileInterface::IMAGES,
            ));


        $builder
            ->add($activity)
            ->add($media);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => WinterActivity::class
        ));
    }


}
