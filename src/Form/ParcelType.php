<?php

namespace App\Form;

use App\Entity\Parcel;
use Doctrine\ORM\EntityManagerInterface;
use NexCRM\BaseBundle\Form\MediaFileInterface;
use NexCRM\BaseBundle\Form\MediaFileMultipleType;
use NexCRM\BaseBundle\Form\MediaFileType;
use NexCRM\BaseBundle\Form\TabType;
use NexCRM\WebBundle\Form\SEOType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;

class ParcelType extends AbstractType
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(
        RequestStack $requestStack,
        EntityManagerInterface $entityManager
    )
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcel = $builder->create('tab_page', TabType::class, array(
            'label' => 'Obsah',
            'icon' => 'file',
            'inherit_data' => true,
            'tab_active' => true,
            'data_class' => Parcel::class
        ));

        $parcel
            ->add('name', TextType::class, [
                "label" => "Název",
                'required' => true,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('totalArea', TextType::class, [
                'label' => 'Velikost pozemku',
                'required' => false
            ])
            ->add('usableArea', TextType::class, [
                'label' => 'Zastavitelná plocha',
                'required' => false
            ])
            ->add('state', ChoiceType::class, [
                'label' => 'Stav',
                'required' => true,
                'choices' => [
                    'Rezervace' => 'Rezervace',
                    'Prodáno' => 'Prodáno',
                    'Volné' => 'Volné'
                ]
            ]);

        $builder
            ->add($parcel);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Parcel'
        ));
    }


}
