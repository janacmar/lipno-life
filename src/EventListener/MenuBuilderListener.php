<?php

namespace App\EventListener;

use NexCRM\BaseBundle\Event\MenuCreateEvent;
use NexCRM\BaseBundle\ValueObject\MenuItem;
use Symfony\Component\Routing\Router;
use NexCRM\BaseBundle\ValueObject\MenuAbstract;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilderListener
{
    /**
     * @var Router
     */
    private $router;

    private $authorizationChecker;

    public function __construct(Router $router, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onBasebundleMenuCreate(MenuCreateEvent $event)
    {

        $menu = $event->getMenu();

        $menu->removeMenuItem($menu->getMenuItemByAlias("web"));
        $menu->removeMenuItem($menu->getMenuItemByAlias("micropage_micropage"));
        $menu->removeMenuItem($menu->getMenuItemByAlias("notify"));
        $menu->removeMenuItem($menu->getMenuItemByAlias("person"));
//        $menu->removeMenuItem($menu->getMenuItemByAlias("micropage_micropage"));

        $activities = (new MenuItem())
            ->setLabel('Aktivity')
            ->setIcon('fa fa-bicycle')
            ->setPriority(89);
        if ($this->authorizationChecker->isGranted('ROLE_SUMMER_ACTIVITY_TEMPLATE_LIST')) {
            $activities->addMenuItem((new MenuItem())
                ->setLabel('Letní aktivity')
                ->setPriority(88)
                ->setRoute($this->router->generate('nexcrm_module_summer_activity_index')));
        }
        if ($this->authorizationChecker->isGranted('ROLE_WINTER_ACTIVITY_TEMPLATE_LIST')) {
            $activities->addMenuItem((new MenuItem())
                ->setLabel('Zimní aktivity')
                ->setPriority(87)
                ->setRoute($this->router->generate('nexcrm_module_winter_activity_index')));
        }

        $menuClass = (new MenuItem())
            ->setAlias('content')
            ->setLabel('Základní obsah')
            ->toggleIsLabelItem()
            ->setPriority(10);

        if ($this->authorizationChecker->isGranted('ROLE_SUMMER_ACTIVITY_TEMPLATE_LIST') || $this->authorizationChecker->isGranted('ROLE_WINTER_ACTIVITY_TEMPLATE_LIST')) {
            $menuClass->addMenuItem($activities);
        }

        $singleMenuItems = [];
        if ($this->authorizationChecker->isGranted('ROLE_WEB_PAGE_TEMPLATE_LIST')) {
            $singleMenuItems[]=['Stránky', 'fa fa-file', 100, 'nexcrm_module_web_page_index'];
        }
        if ($this->authorizationChecker->isGranted('ROLE_PARCEL_TEMPLATE_LIST')) {
            $singleMenuItems[]=['Parcely', 'fa fa-home', 99, 'nexcrm_module_parcel_index'];
        }
        if ($this->authorizationChecker->isGranted('ROLE_SLIDER_TEMPLATE_LIST')) {
            $singleMenuItems[]=['Slidery', 'fa fa-image', 90, 'nexcrm_module_slider_index'];
        }

        $this->setMenu($menu, $menuClass, $singleMenuItems);

        $menuClass = (new MenuItem())
            ->setAlias('others')
            ->setLabel('Ostatní')
            ->toggleIsLabelItem()
            ->setPriority(1);

        if ($this->authorizationChecker->isGranted('ROLE_WEB_TEMPLATE_TEMPLATE_LIST')) {
            $menuClass->addMenuItem((new MenuItem())
                ->setLabel("Šablony")
                ->setIcon('fa fa-map')
                ->setRoute($this->router->generate("nexcrm_module_web_template_index")));
        }

        $menu->addMenuItem($menuClass);
        /*
        $menu = $event->getMenu();

        $menuItem = (new MenuItem())
            ->setLabel("Kategorie")
            ->setIcon('fa fa-file-alt')
            ->setRoute($this->router->generate("nexcrm_module_category_index"))
            ->setPriority(1);

        $menu->getMenuItemByAlias('web')->addMenuItem($menuItem);

        $menuItem = (new MenuItem())
            ->setLabel("Služby")
            ->setIcon('fas fa-external-link-alt')
            ->setRoute($this->router->generate("nexcrm_module_service_index"))
            ->setPriority(3);

        $menu->getMenuItemByAlias('web')->addMenuItem($menuItem);
        */
    }

    function setMenu(MenuAbstract $menu, MenuItem $menuClass, array $singleMenuItems)
    {
        foreach ($singleMenuItems as $singleMenuItem) {
            $menuItem = (new MenuItem())
                ->setLabel((string)$singleMenuItem[0])
                ->setIcon((string)$singleMenuItem[1])
                ->setPriority((int)$singleMenuItem[2])
                ->setRoute($this->router->generate($singleMenuItem[3]));
            $menuClass->addMenuItem($menuItem);
        }

        $menu->addMenuItem($menuClass);
    }
}
