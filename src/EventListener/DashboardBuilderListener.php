<?php

namespace App\EventListener;

use App\Entity\Newsletter;
use Doctrine\ORM\EntityManager;
use NexCRM\BaseBundle\Event\DashboardCreateEvent;
use NexCRM\BaseBundle\ValueObject\DashboardItem;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class DashboardBuilderListener
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        Router $router,
        AuthorizationCheckerInterface $authorizationChecker,
        EntityManager $entityManager
    ) {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->entityManager = $entityManager;
    }

    public function onBasebundleDashboardCreate(DashboardCreateEvent $event)
    {
        $dashboard = $event->getDashboard();
        $newsletters = $this->entityManager->getRepository(Newsletter::class)->findAll();

        $dashboard->addDashboardItem(
            (new DashboardItem())
                ->setAlias("app_export_email")
                ->setLabel("Export e-mailů")
                ->setTemplate("@App/admin/dashboard/newsletter.html.twig")
                ->setData([
                    "newsletterCount" => count($newsletters),
                ])
        );
    }
}