# BUNDLE (vzor project-bundle)

> Aby fungovaly prolinky níže, je nutné aby byl nainstalovaný [nexcrm-project-bundle](https://gitlab.com/nexgenteam/nexcrm-project-bundle)
a [nexcrm-base-bundle](https://gitlab.com/nexgenteam/nexcrm-base-bundle)

Bundly většinou závisí na [`base-bundle`](https://gitlab.com/nexgenteam/nexcrm-base-bundle) (základní bundle), ze kterého dědí,
či používají jeho eventy atd...

## Poznámky

- Začíná-li cesta souboru `/nexcrm/...`, pak je to reference na root projekt.
- Neobsahuje-li cesta souboru `/nexcrm/...`, pak se jedná o soubor z `project-bundle`.
- Obsahuje-li namespace `@[nameBundle]\...` (`@BaseBundle\...`), pak je to
zkratka `NexCRM\BaseBundle\`.

## Gitlab

Jednotlivé bundly se vytvářejí ve skupině [nexgenteam](https://gitlab.com/nexgenteam/) 
podle jm. konvence `nexcrm-[nazev_bundlu]-bundle` ([nexcrm-project-bundle](https://gitlab.com/nexgenteam/nexcrm-project-bundle)).

Každý bundle by měl obsahovat své [readme.md](./../readme.md)
popisující instalaci modulu, featury které přináší, jejich použití...

Nainstalované bundly se nachází v `/nexcrm/vendor/nexcrm/[bundlename]-bundle`

## Základní struktura

```
+-- assets/
|   +-- images
|   +-- js
|       +-- app.js
|   +-- sass
|       +-- app.sass
+-- build/
|   +-- ...
+-- src/
|   +-- Controller/
|   +-- DataFixtures/
|       +-- BundlesFixtures.php
|   +-- DependencyInjection/
|       +-- Configuration.php
|       +-- ProjectExtension.php
|   +-- Entity/
|   +-- EventListener/
|       +-- ControllerListener
|       +-- MenuBuilderListener
|   +-- Form/
|   +-- Repository/
|   +-- Resources/
|       +-- config/
|           +-- parameters.yml
|           +-- routing.yml
|           +-- services.yml
|           +-- templates.yml
|       +-- public/
|       +-- views/
|   +-- Service/
|   +-- Tests/
|   +-- ProjectBundle.php
+-- composer.json
+-- phpunit.xml.dist
+-- postcss.config.js
+-- readme.md
+-- webpack.config.js
+-- package.json
```

- [`/composer.json`](./../vendor/nexcrm/project-bundle/composer.json): Obsahuje php závislosti, jejich verze, privátní repositáře...
- [`/package.json`](./../vendor/nexcrm/project-bundle/package.json): Obsahuje js (npm) závislosti, jejich verze...
- [`/webpack.config.js`](./../vendor/nexcrm/project-bundle/webpack.config.js): Obsahuje nastavení webpacku, cestu k assetům, build složce...
- [`/phpunit.xml.dist`](./../vendor/nexcrm/project-bundle/phpunit.xml.dist): Obsahuje nastavení testů. Cestu k vendor autoloadu...
- [`/readme.md`](./../vendor/nexcrm/project-bundle/readme.md): Instalační instrukce, featury, popis...
- `/build/`: Zde se generuje cache [`@ProjectBundle\Tests\Fixtures\App\AppKernel`](./../vendor/nexcrm/project-bundle/src/Tests/Fixtures/App/AppKernel.php), který používají testy.
- [`/postcss.config.js`](./../vendor/nexcrm/project-bundle/postcss.config.js): Obsahuje nastavení `postcss loaderu` webpacku (autoprefix).

## FrontEnd

### Assets

```
+-- assets/
|   +-- images
|   +-- js
|       +-- app.js
|   +-- sass
|       +-- app.sass
+-- composer.json
+-- postcss.config.js
+-- webpack.config.js
+-- package.json
```

`/assets/`: Obsahuje zdrojáky frontendu (`js`, `sass`, `react`..), obrázky... Složka
se zpracovává [encore](https://symfony.com/doc/3.4/frontend.html) a [webpackem](https://webpack.js.org/)
podle konfigurace uvedené v [`/webpack.config.js`](./../vendor/nexcrm/project-bundle/webpack.config.js).
Zpracování zajistí minifikaci, autoprefixy, kompilaci babelem...

[`/assets/js/app.js`](./../vendor/nexcrm/project-bundle/assets/js/app.js): By default nastavený ve `webpack.config.js` jako
`javascript` Entry point (odtud webpack vytváří graf `javascript` závislostí).

[`/assets/js/sass.js`](./../vendor/nexcrm/project-bundle/assets/js/sass.js): By default nastavený ve `webpack.config.js` jako
`sass` Entry point (odtud webpack vytváří graf `sass` závislostí).

> ProjectBundle zatím neobsahuje žádné assety a tak je `app.js` a `sass.js` prázdné, pro příklad
uvádím odkazy na dané soubory !ALE BaseBundlu!
([`app.js`](./../vendor/nexcrm/base-bundle/assets/js/app.js),[`app.sass`](./../vendor/nexcrm/base-bundle/assets/js/app.sass))

#### Build assetů (minifikace, autoprefixing, caching...)

Zpracuje soubory v `/assets/`, které se vybuildí do `/src/resources/public`, odtud se při `/nexcrm> composer update/install`
(spustí `php bin/console assets:install`) vytvoří symlink do `/nexcrm/web/bundles/[bundlename]/`.

Pro build assetů je potřeba provést uvnitř bundlu `composer install/update`
(nainstaluje mj. nutný `Encore`) a `yarn install/update` (nainstaluje mj. nutný webpack)
a následně samotný build příkaz `yarn encore dev` / `yarn encore dev --watch`.
Pro vývoj se předpokládá, že je git repositář bundlu naklonován v `/nexcrm/bundles/[bundlename]`,
pak při použití `dev` environmentu se assety rovnou vybuildí i do
výše zmíněného `/nexcrm/web/bundles/[bundlename]/`.

Další featurou `webpacku` a `encore` je `manifest.json` sloužící ke cachování.
K názvům assetů se přidává při změně hash, `manifest.json` pak slouží jako
"router", který dle normálního názvu předaného v twigu do funkce
`asset()` směruje na soubor s hashem. Cesta k
manifestu se musí nastavit v [/nexcrm/config.yml](./../app/config/config.yml)

```yml
framework:
    assets:
         # Precte manifest, a poskytne nezacachovany soubor pri pouziti asset() pokud doslo ke zmenam.
         json_manifest_path: '%kernel.project_dir%/web/bundles/base/manifest.json'
```

## BackEnd

### src

```
+-- build/
|   +-- ...
+-- src/
|   +-- Controller/
|   +-- DataFixtures/
|       +-- BundlesFixtures.php
|   +-- DependencyInjection/
|       +-- Configuration.php
|       +-- ProjectExtension.php
|   +-- Entity/
|   +-- EventListener/
|       +-- ControllerListener
|       +-- MenuBuilderListener
|   +-- Form/
|   +-- Repository/
|   +-- Resources/
|       +-- config/
|           +-- parameters.yml
|           +-- routing.yml
|           +-- services.yml
|           +-- templates.yml
|       +-- public/
|       +-- views/
|   +-- Service/
|   +-- Tests/
|   +-- ProjectBundle.php
+-- composer.json
+-- phpunit.xml.dist
```

#### Controller

Obsahuje controllery bundlu. Pokud jsou definovány routy přes anotace,
musí být definovány v [/nexcrm/app/config/routing.yml](./../app/config/routing.yml)
např. následovně:

```yml
_nexcrm_project:
    resource: '@ProjectBundle/Controller/'
    type: annotation
```

Každý controller by měl extendovat `@BaseBundle\Controller\AbstractBaseController`, který
mj. vytváří menu.

Jedná-li se o CRUD controller, je vhodné aby extendoval `@BaseBundle\CRUD\AbstractController`.

#### DataFixtures

```
+-- src/
|   +-- DataFixtures/
|       +-- BundlesFixtures.php
```

[`BundlesFixtures.php`](./../vendor/nexcrm/project-bundle/src/DataFixtures/BundlesFixtures.php) obsahuje fixtury bundlu (defaultní data databáze). Pokud rozšiřuje `Doctrine\Bundle\FixturesBundle\Fixture`
a je pro tuto servicu nastaven `autowire`, pak se data při použití příkazu
`php bin/console doctrine:fixtures:load`, nebo `composer install/update`, který
předešlý příkaz také spouští, data načtou do databáze. Použití je např. definování
security rolí daného bundlu.

#### DependencyInjection

```
+-- src/
|   +-- DependencyInjection/
|       +-- Configuration.php
|       +-- ProjectExtension.php
```

[`Configuration.php`](./../vendor/nexcrm/project-bundle/src/DependencyInjection/Configuration.php): rozšiřuje configy `.yml`.

[`ProjectExtension.php`](./../vendor/nexcrm/project-bundle/src/DependencyInjection/ProjectExtension.php): loaduje (zpracovává a merguje) configy `.yml`, které se
nacházejí v `/src/resources/config/`. Jedná se například o [`services.yml`](./../vendor/nexcrm/project-bundle/src/Resources/config/services.yml).

#### Entity

Obsahuje entity bundlu.

#### EventListener

```
+-- src/
|   +-- EventListener/
|       +-- ControllerListener
|       +-- MenuBuilderListener
```

[`MenuBuilderListener`](./../vendor/nexcrm/project-bundle/src/EventListener/MenuBuilderListener.php)
poslouchá event vytváření menu, který dispatchuje
[`@BaseBundle\Builder\MenuBuilder`](./../vendor/nexcrm/base-bundle/src/Builder/MenuBuilder.php). To umožňuje danému bundlu přidat
do menu své položky.

`MenuBuilderListener` musí být nakonfigurován v [`/src/Resources/config/services.yml`](./../vendor/nexcrm/project-bundle/src/Resources/config/services.yml)

```php
NexCRM\ProjectBundle\EventListener\MenuBuilderListener:
        autowire: true
        autoconfigure: true
        tags:
            - { name: kernel.event_listener, event: basebundle.menu.create }
```

[`ControllerListener`](./../vendor/nexcrm/project-bundle/src/EventListener/ControllerListener.php) definuje cestu k šablonám bundlu (`@ProjectBundle`).

```php
$this->loader->addPath(__DIR__ . "/../../Resources/views", $namespace = 'ProjectBundle');
```

#### Form

Obsahuje formuláře bundlu, případně vlastní FormTypy.

#### Repository

Obsahuje repositáře bundlu. Je vhodné, aby repositáře dědily [`@BaseBundle\Repository\BaseRepository`](./../vendor/nexcrm/base-bundle/src/Repository/BaseRepository.php),
který obsahuje např. základní implementaci metody search (ta se používá v [`@BaseBundle\CRUD\AbstractController`](./../vendor/nexcrm/base-bundle/src/CRUD/AbstractController.php)).

#### Resources

```
|   +-- Resources/
|       +-- config/
|           +-- parameters.yml
|           +-- routing.yml
|           +-- services.yml
|           +-- templates.yml
|       +-- public/
|       +-- views/
```

- `config/` Obsahuje konfiguráky bundlu, které se loadují ve výše zmíněném [`ProjectExtension.php`](./../vendor/nexcrm/project-bundle/src/DependencyInjection/ProjectExtension.php).
- `public/` Obsahuje vybuilděné assety (viz Assets výše).
- `views/` Obsahuje šablony bundlu.

#### Service

Obsahuje `[Name]Template` ([`ProjectTemplate`](./../vendor/nexcrm/project-bundle/src/Service/ProjectTemplate.php))
objekty rozšiřující [`@BaseBundle\Service\AbstractTemplate`](./../vendor/nexcrm/base-bundle/src/Service/AbstractTemplate.php).

`@BaseBundle\Service\AbstractTemplate` obsahuje metodu `setConfig`, která se volá v [`templates.yml`](./../vendor/nexcrm/project-bundle/src/Resources/config/templates.yml).
Předává se jí argument (`%template.project.project%`) z [`parameters.yml`](./../vendor/nexcrm/project-bundle/src/Resources/config/parameters.yml)
obsahující cesty k šablonám.

```yml

// templates.yml

services:
    NexCRM\ProjectBundle\Service\ProjectTemplate:
            calls:
                - method: setConfig
                  arguments:
                    - '%template.project.project%'
```

```yml

// parameters.yml

parameters:
    template.project.project:
        index: "@@ProjectBundle/project/index.html.twig"
        new: "@@ProjectBundle/project/new.html.twig"
        edit: "@@ProjectBundle/project/edit.html.twig"
        show: "@@ProjectBundle/project/show.html.twig"
```



Ty se používají v CRUD controllerech, které dědí [`@BaseBundle\CRUD\AbstractController`](./../vendor/nexcrm/base-bundle/src/CRUD/AbstractController.php),
za účelem získání cesty ke crudovským šablonám bundlu.

```php
class ProjectController extends AbstractController
{
    public function indexAction(Request $request, ProjectTemplate $template): Response
    {
        return $this->list($request, $template->getTemplate('index'));
    }

    ...
}
```

#### Tests

### TODO...







