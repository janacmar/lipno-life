#NEXCRM


[Symfony Best Practices](https://symfony.com/doc/3.4/best_practices/index.html)

## Start

### Install

```bash
composer install
yarn install
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
```

### Update

```bash
composer install
yarn install
php bin/console doctrine:schema:drop --force
php bin/console doc:schem:upd --force
php bin/console doctrine:fixtures:load
```

### Build frontend

```bash
yarn run encore dev
yarn run encore dev --watch
```

```yarn run encore production```

### Test
```php bin/phpunit``` nebo **phpstorm**

### Run
```php bin/console server:run ```

## Dependency configs
- npm/yarn - [package.json](./package.json)
- composer - [composer.json](./composer.json)
- encore webpack - [webpack.config.js](./webpack.config.js)
- postcss: autoprefixing - [postcss.config.js](./postcss.config.js)
- ReactBundle - [config.yml](./app/config/config.yml)

## Symfony bundles

###[Symfony Encore](https://symfony.com/doc/master/frontend.html)
Usnadnuje konfiguraci a praci s webpackem.

###[ReactBundle](https://github.com/Limenius/ReactBundle)
Umoznuje registrovat react componenty a vkladat pomoci twigu `{{ react_component('Component', {'trace': true}) }}` 
(trace zapina vypis console.logu do console pri serverside renderovani).

### Buddy
Používá jiný composer pro instalaci: composer-buddy.json.
