NexCRM - CRUD - Základní použití
================================

Základní CRUD je skládá z abstraktního kontroleru [AppBundle\CRUD\AbstractController](../../src/AppBundle/CRUD/AbstractController.php) a výchozích šablon.

```EntityConfiguration::setKey("employee")``` - employee je klíčové slovo, které je nutné uvádět v routách ve stejné stylu "nexcrm_module_employee_index". Díky němu se dynamicky nastavují přesměrování v AbstractControlleru.

Použití:

```php
<?php

namespace NexCRM\EmployeeBundle\Controller;

use AppBundle\CRUD\AbstractController;
use AppBundle\CRUD\EntityConfiguration;
use Doctrine\ORM\EntityManager;
use NexCRM\EmployeeBundle\Entity\Employee;
use NexCRM\EmployeeBundle\Form\EmployeeType;
use NexCRM\EmployeeBundle\Service\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;

/**
 * @Breadcrumb({
 *   { "label" = "$entityConfiguration.label[index]", "route" = "nexcrm_module_employee_index" }
 * })
 */
class DefaultController extends AbstractController
{

    public function __construct(EntityManager $entityManager)
    {
        $entityConfiguration = (new EntityConfiguration())
            ->setName(Employee::class)
            ->setKey("employee")
            ->setFormType(EmployeeType::class)
            ->setProperties([
                "position" => "Pozice",
                "user" => "Uživatel"
            ])
            ->setLabel([
                "index" => "Zaměstnanec",
                "new" => "Nový zaměstnanec",
                "show" => "Zobrazení zaměstnance",
                "edit" => "Úprava zaměstnance",
                "notFound" => "Žádný zaměstnanec nenalezen.",
                "delete" => "Opravdu chcete zaměstnance smazat?",
                "icon" => "fa fa-lock"
            ]);

        parent::__construct($entityManager, $entityConfiguration);
    }

    /**
     * @Route("/employee", name="nexcrm_module_employee_index")
     * @return Response
     */
    public function indexAction(Request $request, Template $template): Response
    {
        return $this->list($request, $template->getIndex());
    }

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[new]" }
     * })
     * @Route("/employee/new", name="nexcrm_module_employee_new")
     */
    public function newAction(Request $request, Template $template)
    {
        return $this->new($request, $template->getNew());
    }
    
    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[show]" }
     * })
     * @Route("/employee/show", name="nexcrm_module_employee_show")
     */
    public function showAction(Request $request, Template $template)
    {
        return $this->show($request, $template->getShow());
    }    

    /**
     * @Breadcrumb({
     *   { "label" = "$entityConfiguration.label[edit]" }
     * })
     * @Route("/employee/edit/{entity_id}", name="nexcrm_module_employee_edit")
     */
    public function editAction(Request $request, Template $template, $entity_id)
    {
        return $this->edit($request, $template->getEdit(), $entity_id);
    }

    /**
     * @Route("/employee/remove/{entity_id}", name="nexcrm_module_employee_remove")
     */
    public function removeAction(Request $request, $entity_id)
    {
        return $this->remove($request, $entity_id);
    }
}
```

Defakto takový kontroler slouží pro 2 věci:
    1. nastavení rout
    2. nastavení šablon
    
Šablony
-------

V bundlu šablony můžeme přetížit

list.html.twig:

```twig
{% extends "crud/list.html.twig" %}
```

new.html.twig:

```twig
{% extends "crud/form.html.twig" %}
```

edit.html.twig:

```twig
{% extends "crud/form.html.twig" %}
```

show.html.twig:

```twig
{% extends "crud/form.html.twig" %}
```
