Deployment
==========

Nasazování probíhá automaticky přes http://buddy.nexgen.cz.

Pokud bundle vydá aktualizaci spustí s pipelina daného bundlu a
 1. Spustí se PHPUnit testy, statická analýza a PHP Code Sniffer - PSR2
 2. Pokud projdou dojde trigger akci, které spustí nasazování CRM
 
Samotné CRM má proces následující:
 1. Nahrají se úpravy na FTP
 2. Spustí se akce přes SSH
    1. Smaže se cache ```rm -rf var/cache/*```
    2. Spustí se composer bez skriptů ```composer update --prefer-source --no-scripts```
    3. Spustí se instalace assetů
    4. Smaže se databáze, spustí DataFixtures