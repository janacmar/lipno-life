NexCRM
======

Instalace
---------

```cmd
composer install
yarn install
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
```


Aktualizace
-----------

```cmd
composer update
yarn install
php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load
```

CRUD
----

[Základní použití](./CRUD/zaklady.md)