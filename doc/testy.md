Testy
=====

Testy z důvodu Buddyho, kde MySQL běží jako služba v Dockeru, musí mí jako host nastaven místo "localhost" - "mysql".

Stačí v /etc/hosts u sebe nastavit

```
127.0.0.1 localhost
127.0.0.1 mysql
```

Následně v příkazové řádce bundlu stačí spustit:

```cmd
vendor/bin/phpunit --configuration phpunit.xml.dist ./src/Tests
vendor/bin/phpstan analyse -l 1 src
vendor/bin/phpcs --standard=PSR2 --ignore=src/Resources src
```

Pokud je bundle tak nastaven (zatím jen BaseBundle), stačí jenom zkratky

```cmd
bin/phpunit
bin/phpstan
bin/phpcs
```

Případně i nejjednodušeji vše najendou

```cmd
composer test
```