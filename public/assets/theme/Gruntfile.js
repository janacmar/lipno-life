module.exports = function (grunt) {
    var theme = '.';
    var modules = 'node_modules/';

    const sass = require('node-sass');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    modules + 'jquery/dist/jquery.js',
                    //modules + 'bootstrap/dist/js/bootstrap.bundle.js',
                    modules + 'slick-carousel/slick/slick.js',
                    modules + '@fancyapps/fancybox/dist/jquery.fancybox.js',
                    modules + 'aos/dist/aos.js'
                    //modules + 'select2/dist/js/select2.full.js'
                ],
                dest: theme + '/js/base/_npm.js'
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd h:MM:ss") %> */\n'
            },
            build: {
                src: [theme + '/js/*/*.js', theme + '/js/script.js'],
                dest: theme + '/dist/all.min.js'
            }
        },

        sass: {
            options: {
                implementation: sass,
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd h:MM:ss") %> */\n',
                style: 'expanded'
            },
            dist: {
                files: {
                    "css/main_sass.css": theme + '/sass/main.sass'
                }
            }
        },

        cssmin: {
            build: {
                src: [theme + '/css/*.css'],
                dest: theme + '/dist/all.min.css'
            }
        },

        postcss: {
            options: {
                processors: [
                    require('pixrem')(),
                    require('autoprefixer')({browsers: 'last 2 versions'})
                ]
            },
            dist: {
                src: theme + '/dist/all.min.css'
            }
        },

        watch: {
            options: {
                livereload: true,
                event: ['changed', 'added', 'deleted']
            },
            html: {
                files: [theme + '/*.php', theme + '/*.html', theme + '/*/*.php', theme + '/*/*.html', theme + '/*/*.latte', theme + '/*/*.latte'],
                tasks: []
            },
            sass: {
                files: [theme + '/sass/*.sass', theme + '/sass/*/*.sass', theme + '/sass/*.scss', theme + '/sass/*/*.scss', theme + '/sass/*/*/*.sass'],
                tasks: ['sass']
            },
            css: {
                files: [theme + '/css/*.css'],
                tasks: ['cssmin', 'postcss']
            },
            script: {
                files: [theme + '/js/*.js', theme + '/js/*/*.js'],
                tasks: ['uglify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'cssmin:build', 'postcss', 'watch']);
    grunt.registerTask('prod', ['concat', 'uglify', 'sass', 'cssmin:build', 'postcss']);
};