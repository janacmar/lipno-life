$(function () {

    initParcel();
    $('.parcel__number span').on('click', function () {
        var screenWidht = document.documentElement.clientWidth;
        if (screenWidht > 991) {
            $('.parcel__number span.is-active, .parcel__number.is-active').removeClass('is-active');
            $(this).toggleClass('is-active');
            $(this).parent('.parcel__number').toggleClass('is-active');
            e.stopPropagation()
        }
    });
    $(document).on('click', function (e) {
        if ($('.parcel__number span').is(e.target) === false) {
            $('.parcel__number.is-active').removeClass('is-active');
            $('.parcel__number span.is-active').removeClass('is-active');
        }

    });
    var max = parseInt($('#display--button').data('item-max'));
    var counter = 0;
    var tableItems = $('[data-parcel-name-src]');
    tableItems.each(function () {
        var item = $(this);
        if (counter >= max) {
            item.addClass('is-hidden');
        }
        counter++;
    });
});

$(window).resize(initParcel);

function initParcel() {
    var parcelItem = $('.parcel__item');
    var parcelContent = $('.parcel__content');
    parcelContent.empty();
    parcelItem.each(function () {
        var item = $(this);
        var parcelName = item.data('parcel-name');
        var tableItem = $('[data-parcel-name-src="' + parcelName + '"]');
        var path = this.getBoundingClientRect();
        var pathWidth = path.width;
        var pathHeight = path.height;

        var mapPos = $('.parcel__img');
        var mapTop = mapPos.offset().top;
        var mapLeft = mapPos.offset().left;
        var itemTop = item.offset().top;
        var itemLeft = item.offset().left;
        var posTop = itemTop - mapTop;
        var posLeft = itemLeft - mapLeft;
        var tableHtml = tableItem.html();
        var tableAvailable = tableItem.data('table-available');
        var pathTop = path.top - mapTop;
        var pathLeft = path.left - mapLeft;

        if (tableItem.data('table-available') === 'available') {
            item.addClass('is-free');
        } else if (tableItem.data('table-available') === 'reservation') {
            item.addClass('is-reservation');
        } else {
            item.addClass('is-sold');
            tableItem.addClass('is-disabled');
        }

        var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        if (isSafari) {
            parcelContent.append(
                '<div class="parcel__number" data-content-available="'+ tableAvailable +'" data-content-name-src="'+ parcelName +'" ' +
                'style="width: '+ pathWidth +'px; height: '+ pathHeight +'px; top: '+ pathTop +'px; left: '+ pathLeft +'px"><span>'+ parcelName +'</span>' +
                '<div class="parcel__cnt"><div class="parcel__cnt__item"><table><tr>'+ tableHtml +'</tr></table></div></div</div>');
        }
        else {
            parcelContent.append(
                '<div class="parcel__number" data-content-available="'+ tableAvailable +'" data-content-name-src="'+ parcelName +'" ' +
                'style="width: '+ pathWidth +'px; height: '+ pathHeight +'px; top: '+ posTop +'px; left: '+ posLeft +'px"><span>'+ parcelName +'</span>' +
                '<div class="parcel__cnt"><div class="parcel__cnt__item"><table><tr>'+ tableHtml +'</tr></table></div></div</div>');
        }


    });
}