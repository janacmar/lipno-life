$(function () {
    $('[data-table-available-src="available"]').change(function() {
        $('[data-content-available="unavailable"], [data-content-available="reservation"]').toggleClass('is-hidden', this.checked);
        $('.parcel__item.is-reservation, .parcel__item.is-sold').toggleClass('is-hidden', this.checked);
        $('[data-content-available="available"]').toggleClass('is-show', this.checked);
        if ($('.clicked').length) {
            $('[data-table-available="unavailable"], [data-table-available="reservation"]').toggleClass('is-hidden', this.checked);
        }
        else {
            var max = parseInt($('#display--button').data('item-max'));
            if (this.checked) {
                var counter = 0;
                var tableItems = $('[data-parcel-name-src]');
                tableItems.each(function () {
                    var item = $(this);
                    item.removeClass('is-hidden');
                    if (counter >= max || item.data('table-available') !== 'available') {
                        item.addClass('is-hidden');
                    }
                    else {
                        counter++;
                    }
                });
            }
            else {
                var counter = 0;
                var tableItems = $('[data-parcel-name-src]');
                tableItems.each(function () {
                    var item = $(this);
                    item.removeClass('is-hidden');
                    if (counter >= max) {
                        item.addClass('is-hidden');
                    }
                    counter++;
                });
            }
        }
    });
});