$(function () {
    $('.js-burger').on('click', function () {
        $(this).toggleClass('is-active');
        $('.nav').toggleClass('is-open');
        $('body').toggleClass('is-fixed');
    });
    $('.nav li').on('click', function () {
        var screenWidht = document.documentElement.clientWidth;
        if (screenWidht < 991) {
            $('.js-burger').toggleClass('is-active');
            $('.nav').toggleClass('is-open');
            $('body').toggleClass('is-fixed');
        }
    });
});