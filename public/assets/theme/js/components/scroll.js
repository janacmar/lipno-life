$(function () {
    var anchor = $("[data-scroll-src]");

    var anchor_hash = window.location.hash.substring(1);

    if (anchor_hash !== "") {
        setTimeout(function () {
            scrollToAnchor(anchor_hash);
        }, 200);
    }

    anchor.each(function () {
        if ($(this).attr('href') !== undefined) {
            $(this).attr('href', "#" + $(this).data("scroll-src"));
        }
    });

    anchor.on("click", function (e) {
        var el = $(this).data("scroll-src");
        scrollToAnchor(el);
        e.preventDefault();
        $("[data-scroll-src].is-active").removeClass('is-active');
        $(this).addClass('is-active');

        if (typeof $("[data-scroll='" + el + "']").data('anchor-no-url') !== 'undefined') {
            history.pushState(null, null, '#' + el);
        }
    });

    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var hash = window.location.hash.substring(1);
            history.pushState(null, null, '#' + hash);
            scrollToAnchor(hash);
        }
    });
});

function scrollToAnchor(el) {
    var anchor_offset = 115;
    var element = $("[data-scroll='" + el + "']");
    var anchor_scroll = element.offset().top;
    var scroll_size = anchor_scroll - anchor_offset;

    $('html, body').animate({
        scrollTop: scroll_size
    }, 1000);
}

$(document).ready(function () {
    $(".footer__scroll").click(function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
});

$(window).on("load", function () {
});

function scrolling() {
    var height = $(window).scrollTop();
    // $(window).height()
    if (height > 100) {
        $('.footer__scroll').fadeIn();
    } else {
        $('.footer__scroll').fadeOut();
    }
}

$(document).on('scroll', scrolling);


$(window).scroll(function(event) {
    function footer()
    {
        var scroll = $(window).scrollTop();
        if(scroll>30)
        {
            $(".footer__scroll").fadeIn("slow").addClass("show");
        }
        else
        {
            $(".footer__scroll").fadeOut("slow").removeClass("show");
        }

        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function() {
            if ($('.footer__scroll').is(':hover')) {
                footer();
            }
            else
            {
                $(".footer__scroll").fadeOut("slow");
            }
        }, 2000));
    }
    footer();
});
