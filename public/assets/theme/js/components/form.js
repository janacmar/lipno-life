$(function () {

    $('.js-form input, .js-form textarea').on('change', function () {
        $(this).val() !== '' ? $(this).addClass('has-content') : $(this).removeClass('has-content');
    });

});