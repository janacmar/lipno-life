$(document).ready(function () {
    var screenWidth = document.documentElement.clientWidth;

    var scrollRef = 0;
    window.addEventListener('scroll', function () {
        scrollRef <= 10 ? scrollRef++ : AOS.refresh();
    });

    if (screenWidth > 1200) {
        AOS.init({
            offset: 150,
            once: true,
            duration: 500
            // disable: true
        });
    }
    else  {
        AOS.init({
            disable: true
        });
    }


});