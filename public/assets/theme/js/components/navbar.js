$(function () {
    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("js-navbar");

    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= 380) {
            navbar.classList.add("is-sticky")
        } else {
            navbar.classList.remove("is-sticky");
        }
    }
});
