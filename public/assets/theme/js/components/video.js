$(function () {
    $(document).on('click', '[data-video]', function (e) {
        e.preventDefault();
        var poster = $(this);
        var wrapper = poster.parent();
        wrapper.addClass("video--play");
        videoPlay(wrapper);
    });
});

function videoPlay(wrapper) {
    var iframe = wrapper.find("iframe");
    var src = iframe.data('src');
    iframe.attr('src', src);
}