/**
 * <a href="#" data-link-to=".anchor-1"></a>
 * <div class="anchor-1"></div>
 * js: new LinkTo(-30);
 */

var LinkTo = function (plus) {

    var self = this;

    if (typeof plus === "undefined") plus = 0;

    this.plus = plus;
    this.anchors = [];

    $("a").each(function () {
        var link_to = $(this).data("link-to");
        if (link_to === "" || typeof link_to === "undefined") return;

        self.anchors.push(link_to);

        $(this).on("click", function () {
            if (link_to === ".demo") $(link_to).css("opacity", 1).show();
            if (!$(link_to).offset()) return;
            var top = $(link_to).offset().top;
            $("html, body").stop().animate({scrollTop: top + self.plus}, 1000);

            return false;
        });
    });

    this.setScroll();
};

LinkTo.prototype.setScroll = function () {

    var self = this;

    $(document).on('scroll', function () {
        var top, max_top, anchor, scrollTop;
        var a = $("a");
        max_top = 0;
        scrollTop = get_scrollTop();

        for (var i in self.anchors) {
            anchor = $(self.anchors[i]);
            top = anchor.offset().top;
            if (top < max_top) {
                continue;
            }
            if (top + self.plus - 1 <= scrollTop) {
                max_top = top;

                a.each(function () {
                    var link_to = $(this).data("link-to");
                    if (link_to === "" || typeof link_to === "undefined") return;
                    $(this).parents("li").removeClass("active");
                });

                a.each(function () {
                    var link_to = $(this).data("link-to");
                    if (link_to === "" || typeof link_to === "undefined" || link_to !== self.anchors[i]) return;
                    if (!$(this).hasClass("nonposibble-active")) {
                        $(this).parents("li").addClass("active");
                    }
                });
            }
        }
    });
};