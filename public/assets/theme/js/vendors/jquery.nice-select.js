(function ($) {

    $.fn.niceSelect = function (method) {

        // Methods
        if (typeof method == 'string') {
            if (method == 'update') {
                this.each(function () {
                    var $select = $(this);
                    var $dropdown = $(this).next('.select');
                    var open = $dropdown.hasClass('open');

                    if ($dropdown.length) {
                        $dropdown.remove();
                        create_nice_select($select);

                        if (open) {
                            $select.next().trigger('click');
                        }
                    }
                });
            } else if (method == 'destroy') {
                this.each(function () {
                    var $select = $(this);
                    var $dropdown = $(this).next('.select');

                    if ($dropdown.length) {
                        $dropdown.remove();
                        $select.css('display', '');
                    }
                });
                if ($('.select').length == 0) {
                    $(document).off('.nice_select');
                }
            } else {
                console.log('Method "' + method + '" does not exist.')
            }
            return this;
        }

        // Hide native select
        this.hide();

        // Create custom markup
        this.each(function () {
            var $select = $(this);

            if (!$select.next().hasClass('select')) {
                create_nice_select($select);
            }
        });

        function create_nice_select($select) {
            $select.after($('<div></div>')
                .addClass('select')
                .addClass($select.attr('class') || '')
                .addClass($select.attr('disabled') ? 'disabled' : '')
                .attr('tabindex', $select.attr('disabled') ? null : '0')
                .html('<span class="select__current"></span><ul class="select__list"></ul>')
            );

            var $dropdown = $select.next();
            var $options = $select.find('option');
            var $selected = $select.find('option:selected');

            $dropdown.find('.select__current').html($selected.data('display') || $selected.text());

            $options.each(function (i) {
                var $option = $(this);
                var display = $option.data('display');
                var style = $option.data('style');//HH
                var custom_class = $option.attr("class");//HH
                var tip = $option.data('tip');//HH
                var input = $option.data('input') || false;
                var input_value = $option.data('input-value') || "";
                var input_placeholder = $option.data('input-placeholder') || "";

                $dropdown.find('ul').append($('<li></li>')
                    .attr('data-value', $option.val())
                    .attr('data-display', (display || null))
                    .attr('data-input', (input || null))
                    .attr('data-input-value', (input_value || null))
                    .attr('data-input-placeholder', (input_placeholder || null))
                    .attr('style', (style || null))
                    .attr('class', (custom_class || null))
                    .addClass('select__option' +
                        ($option.is(':selected') ? ' selected' : '') +
                        ($option.is(':disabled') ? ' disabled' : ''))
                    .html($option.text() + (tip ? (' <span>' + tip + '</span>') : ""))
                );

                //HH add input
                if (input && $option.is(':selected')) {
                    $dropdown.find('.select__current').html("<label>" + input_placeholder + "</label><input type='number' name='" + input + "' value='" + input_value + "' placeholder='" + $option.text() + "' class='select__input'>");
                }

            });
        }

        /* Event listeners */

        // Unbind existing events in case that the plugin has been initialized before
        $(document).off('.select');

        // Open/close
        $(document).on('click.select', '.select', function (event) {
            var $dropdown = $(this);

            $('.select').not($dropdown).removeClass('open');
            $dropdown.toggleClass('open');

            if ($dropdown.hasClass('open')) {
                $dropdown.find('.select__option');
                $dropdown.find('.focus').removeClass('focus');
                $dropdown.find('.selected').addClass('focus');
            } else {
                $dropdown.focus();
            }
        });

        // Close when clicking outside
        $(document).on('click.select', function (event) {
            if ($(event.target).closest('.select').length === 0) {
                $('.select').removeClass('open').find('.select__option');
            }
        });

        // Option click
        $(document).on('click.select', '.select .select__option:not(.disabled)', function (event) {
            var $option = $(this);
            var $dropdown = $option.closest('.select');

            $dropdown.find('.selected').removeClass('selected');
            $option.addClass('selected');

            var text = $option.data('display') || $option.text();
            var input = $option.data('input') || false;
            var input_value = $option.data('input-value') || "";
            var input_placeholder = $option.data('input-placeholder') || "";

            $dropdown.find('.select__current').text(text);

            //HH add input
            if (input) {
                $dropdown.find('.select__current').html("<label>" + input_placeholder + "</label><input type='number' name='" + input + "' placeholder='" + text + "' value='" + input_value + "' class='select__input'>");
            }

            $dropdown.prev('select').val($option.data('value')).trigger('change');
        });

        // Keyboard events
        $(document).on('keydown.select', '.select', function (event) {
            var $dropdown = $(this);
            var $focused_option = $($dropdown.find('.focus') || $dropdown.find('.select__list .select__option.selected'));

            // Space or Enter
            if (event.keyCode == 32 || event.keyCode == 13) {
                if ($dropdown.hasClass('open')) {
                    $focused_option.trigger('click');
                } else {
                    $dropdown.trigger('click');
                }
                return false;
                // Down
            } else if (event.keyCode == 40) {
                if (!$dropdown.hasClass('open')) {
                    $dropdown.trigger('click');
                } else {
                    var $next = $focused_option.nextAll('.select__option:not(.disabled)').first();
                    if ($next.length > 0) {
                        $dropdown.find('.focus').removeClass('focus');
                        $next.addClass('focus');
                    }
                }
                return false;
                // Up
            } else if (event.keyCode == 38) {
                if (!$dropdown.hasClass('open')) {
                    $dropdown.trigger('click');
                } else {
                    var $prev = $focused_option.prevAll('.select__option:not(.disabled)').first();
                    if ($prev.length > 0) {
                        $dropdown.find('.focus').removeClass('focus');
                        $prev.addClass('focus');
                    }
                }
                return false;
                // Esc
            } else if (event.keyCode == 27) {
                if ($dropdown.hasClass('open')) {
                    $dropdown.trigger('click');
                }
                // Tab
            } else if (event.keyCode == 9) {
                if ($dropdown.hasClass('open')) {
                    return false;
                }
            }
        });

        // Detect CSS pointer-events support, for IE <= 10. From Modernizr.
        var style = document.createElement('a').style;
        style.cssText = 'pointer-events:auto';
        if (style.pointerEvents !== 'auto') {
            $('html').addClass('no-csspointerevents');
        }

        return this;

    };

}(jQuery));