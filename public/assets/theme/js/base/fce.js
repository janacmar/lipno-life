function get_scrollTop() {
    if ($("html").scrollTop()) return $("html").scrollTop();
    if ($("body").scrollTop()) return $("body").scrollTop();
    return 0;
}


function rendered() {

}

function startRender() {
    requestAnimationFrame(rendered);
}

function loaded() {
    requestAnimationFrame(startRender);
}

function isMobile() {
    return window.matchMedia('(max-width: 1049px)').matches;
}